package dev.lion.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={
        "dev.lion.helloworld"
})
public class HelloWorldBootApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(HelloWorldBootApplication.class, args);
    }
}
