package dev.lion.helloworld.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/helloworld")
public class HelloWorldResource {
    //
    private static Map<String, String> names = new HashMap<>();

    @PostMapping
    public String registerName(HttpServletRequest request, @RequestBody String name) {
        //
        names.put(request.getSession().getId(), name);
        return "Successfully registered name as " + name;
    }

    @PutMapping
    public String modifyName(HttpServletRequest request, @RequestBody String name) {
        //
        String oldName = Optional.ofNullable(names.get(request.getSession().getId())).orElse("anonymous");
        names.put(request.getSession().getId(), name);
        return "Successfully modified name as " + name + " from " + oldName;
    }

    @DeleteMapping
    public String deleteName(HttpServletRequest request) {
        //
        String oldName = Optional.ofNullable(names.get(request.getSession().getId())).orElse("anonymous");
        return "Successfully deleted name as " + oldName;
    }

    @GetMapping
    public String sayHello(HttpServletRequest request) {
        //
        String name = Optional.ofNullable(names.get(request.getSession().getId())).orElse("anonymous");
        return "Hello. " + name;
    }
}
