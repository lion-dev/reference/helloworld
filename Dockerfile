FROM openjdk:11-jdk
VOLUME /tmp
COPY helloworld-boot/build/libs/helloworld-boot-0.1.0-SNAPSHOT.jar app.jar
RUN bash -c 'touch /app.jar'
# "-Djava.security.egd=file:/dev/./urandom"
ENTRYPOINT ["java", "-Dspring.profiles.active=cloud", "-jar","/app.jar"]